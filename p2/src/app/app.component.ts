import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'p2';

  public m: number = 6;
  public n: number = 5;
  private milisegundos: number = 0;

  public multi: any = new Array<any>();
  public disabledBtn = false;
  public msg: string = '';

  public run() {

    this.disabledBtn = true;
    this.multi = [];
    this.milisegundos = 0;
    this.msg = '';

    for (let i = 0; i < this.n; i++) {

      this.multi.push([]);

      for (let j = 0; j < this.m; j++) {

        this.multi[i].push(false);


      }
    }

    let naux = 0;
    let maux = 0;

    let position = 'R';

    if (this.m === 1) {
      position = 'D';
    }

    let rlimit = this.m - 1;
    let dlimit = this.n - 1;
    let llimit = 0;
    let ulimit = 1;

    for (let i = 0; i < (this.n * this.m); i++) {

            this.setposition(naux, maux);

            switch (position) {
              case 'R':
                maux++;

                if (maux === rlimit ) {
                  position = 'D';
                  rlimit--;
                }
                break;

                case 'D':
                  naux++;

                  if (naux === dlimit ) {
                     position = 'L';
                     dlimit--;
                  }
                  break;


                  case 'L':
                    maux--;

                    if (maux === llimit ) {
                       position = 'U';
                       llimit++;
                    }
                    break;

                    case 'U':
                      naux--;

                      if (naux === ulimit ) {
                         position = 'R';
                         ulimit++;

                      }
                      break;


            }


      }


    setTimeout(() => {

      this.disabledBtn = false;


   }, this.milisegundos);


  }

  setposition(i: number, j: number) {

    setTimeout(() => {

      this.multi[i][j] = true;
      console.log(i + ',' + j);
      // this.msg = this.msg + i + ',' + j + ' --> ';


   }, this.milisegundos);

    this.milisegundos = this.milisegundos + 100;



  }

  clear() {
    this.msg = '';
    this.multi = [];
  }


}
