# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Requisitos ###

* Editor de texto, se recomienda Visual Studio Code.

### Para ejecutar el proyecto ###

* Descargar el proyecto.
* Ubicarse en la carpeta raíz desde la consola del editor de texto
* Ejecutar el comando: npm start
* Abrir un navegador e ir la dirección: http://localhost:4200 

### Autor ###

* Armando Iván López May
* lopezpay07@gmail.com
* 9991642250